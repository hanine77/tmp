*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}

*** Test Cases ***
TestOrange
    [Documentation]   Checks accessibility to https://opensource-demo.orangehrmlive.com/
    #In SeleniumLibrary, browser is a synonym for WebDriver instance.!
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    #Windows are children of a browser
    Location Should Contain     ${Global.Url}
    Click Element       xpath://input[@name='Username']
    Input Text      name:Username     Admin
    Click Element       xpath://input[@name='Password']
    Input Text      name:Password      admin123
    Click button        xpath://input[@value='press me']
    Capture page Screenshot

      
    #page should contain textfield   cd
    #Click Element     //*[@id="app"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input
    #Input Text     id:app      Admin
    #Click Element       //*[@id="app"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input
    #Input Text      id:app     admin123
    #Click button       //*[@id="app"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button
 
    #Click Element     name:"username"
    #Input Text     name:username     Admin
    #Click Element       name:"password"
    #Input Text      name:password    admin123
    #Click Element       name:Login
    Close Browser



