*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}


*** Test Cases ***
TestGoogle
    [Documentation]   Checks accessibility to https://www.google.fr
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    Maximize Browser Window
    Location Should Contain    ${Global.Url}
    # Cliquer sur le bouton reject all de cookie
    Click Element    //*[@id="APjFqb"]
    Input Text    id=APjFqb   AirFrance
    Close Browser
